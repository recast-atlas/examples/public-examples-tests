# Public RECAST ATLAS example tests

In an effort to automatically detect issues like [`recast-atlas/susy/ATLAS-CONF-2018-041` Issue #1](https://gitlab.cern.ch/recast-atlas/susy/ATLAS-CONF-2018-041/-/issues/1) before they become problems, this project runs public example workflows in CI on a CRON schedule.

## RECAST ATLAS catalogue entries and specs

This project runs the example RECAST catalogue entry files included with `recast-atlas` distributions.
The actual RECAST catalogue entry files and associated specs used are:

* [`examples/rome`](https://github.com/recast-hep/recast-atlas/blob/de61902bc6a66104965cced12471a8f195075bb3/src/recastatlas/data/catalogue/examples_rome.yml)
   - Associated specs: https://github.com/reanahub/reana-demo-atlas-recast
* [`atlas/atlas-conf-2018-041`](https://github.com/recast-hep/recast-atlas/blob/de61902bc6a66104965cced12471a8f195075bb3/src/recastatlas/data/catalogue/atlas_atlas_conf_2018_041.yml)
   - Associated specs: https://gitlab.cern.ch/recast-atlas/susy/ATLAS-CONF-2018-041
